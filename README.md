[![Institut Maupertuis logo](http://www.institutmaupertuis.fr/media/gabarit/logo.png)](http://www.institutmaupertuis.fr)

- Do NOT use a `aarch64` (64 bits) image of Raspberry Pi OS/ Ubuntu
- [Documentation](./documentation.md)

# Dependencies
- [wiringPi](https://github.com/WiringPi/WiringPi)

# CMake variables
- `flir_lepton_ROSMATER_HOSTNAME`: Specify to which ROS master the node will connect
- `flir_lepton_TOPIC`: Specify the topic on which images will be published

# Flir Lepton
Tested with:
- Breakout 1.4
- Lepton 2.5

Maximum refresh rate is 9 Hz: fetching images faster result in duplicate images.

If the Lepton module has a shutter the image will sometimes stop being published for a short time (~0.5 sec) during the Flat Field Correction (FFC) correction.

## Wiring
- I2C must be wired.
- SPI must be wired, chip select must `CE1` instead of `CE0`
- Transistor base: Wiring Pi pin 2 (BCM 27). The transistor is used to power on/off the Flir Lepton to power reboot it

## Allow acces to SPI, I2C and GPIO without superuser privileges
```bash
echo 'SUBSYSTEM=="spidev", GROUP="spiuser", MODE="0660"' | sudo tee /etc/udev/rules.d/50-spi.rules
sudo groupadd spiuser
sudo adduser $USER spiuser
```

```bash
echo 'SUBSYSTEM=="i2c-dev", GROUP="i2cuser", MODE="0660"' | sudo tee /etc/udev/rules.d/50-i2c.rules
sudo groupadd i2cuser
sudo adduser $USER i2cuser
```

```bash
sudo groupadd gpio
sudo adduser $USER gpio
sudo chown root.gpio /dev/gpiomem
sudo chmod g+rw /dev/gpiomem
```

Logout and login to make the changes effective.

# Output images
- Output images are grayscale 16 bit images.
- Flir Lepton pixels are coded on 14 bits (0 to 16 383).

In RViz tick the `Normalize range` option to easily view a thermal image.

# Usage
Run the node on the Raspberry Pi:
```bash
rosrun flir_lepton raw_publisher
```

# Automatic startup
```bash
sudo cp build/flir_lepton/scripts/ros_flir_lepton.service /etc/systemd/system
sudo systemctl enable ros_flir_lepton
sudo systemctl start ros_flir_lepton
```
