#include <flir_lepton/panels/lepton.hpp>

namespace flir_lepton
{

Lepton::Lepton(QWidget *parent) :
  rviz::Panel(parent)
{
  connect(this, &Lepton::enable, this, &Lepton::setEnabled);
  setObjectName("Lepton");
  setName(objectName());

  qRegisterMetaType<QMessageBox::Icon>();
  connect(this, &Lepton::displayMessageBox, this, &Lepton::displayMessageBoxHandler);

  QVBoxLayout *layout(new QVBoxLayout);
  QWidget *scroll_widget = new QWidget;
  scroll_widget->setLayout(layout);
  QScrollArea *scroll_area = new QScrollArea;
  scroll_area->setWidget(scroll_widget);
  scroll_area->setWidgetResizable(true);
  scroll_area->setFrameShape(QFrame::NoFrame);
  QVBoxLayout *main_layout = new QVBoxLayout(this);
  main_layout->addWidget(scroll_area);

  QHBoxLayout *base_topic_layout(new QHBoxLayout);
  base_topic_line_edit_ = new QLineEdit;
  base_topic_line_edit_->setText("/flir_lepton");
  change_base_topic_ = new QPushButton();
  change_base_topic_->setIcon(QIcon::fromTheme("dialog-ok-apply"));
  base_topic_layout->addWidget(new QLabel("Base topic: "));
  base_topic_layout->addWidget(base_topic_line_edit_);
  base_topic_layout->addWidget(change_base_topic_);

  QHBoxLayout *fpa_temp_layout(new QHBoxLayout);
  QDoubleSpinBox *fpa_temperature(new QDoubleSpinBox);
  fpa_temperature->setReadOnly(true);
  fpa_temperature->setSuffix(" °C");
  fpa_temperature->setDecimals(1);
  fpa_temp_layout->addWidget(new QLabel("FPA temperature: "));
  fpa_temp_layout->addWidget(fpa_temperature);

  QHBoxLayout *aux_temp_layout(new QHBoxLayout);
  QDoubleSpinBox *aux_temperature(new QDoubleSpinBox);
  aux_temperature->setReadOnly(true);
  aux_temperature->setSuffix(" °C");
  aux_temperature->setDecimals(1);
  aux_temp_layout->addWidget(new QLabel("AUX temperature: "));
  aux_temp_layout->addWidget(aux_temperature);

  QHBoxLayout *gain_layout(new QHBoxLayout);
  gain_layout->addWidget(new QLabel("Gain mode: "));
  QComboBox *gain_mode(new QComboBox);
  gain_mode->addItem("High");
  gain_mode->addItem("Low");
  gain_mode->addItem("Auto");
  gain_layout->addWidget(gain_mode);

  QHBoxLayout *shutter_mode_layout(new QHBoxLayout);
  shutter_mode_layout->addWidget(new QLabel("Shutter mode (FFC): "));
  QComboBox *shutter_mode(new QComboBox);
  shutter_mode->addItem("Manual");
  shutter_mode->addItem("Auto");
  shutter_mode->addItem("External");
  shutter_mode_layout->addWidget(shutter_mode);

  QHBoxLayout *bottom_buttons(new QHBoxLayout);
  QPushButton *run_ffc_normalization(new QPushButton("FFC normalization"));
  QPushButton *run_oem_reboot(new QPushButton("OEM reboot"));
  bottom_buttons->addWidget(run_ffc_normalization);
  bottom_buttons->addWidget(run_oem_reboot);

  status_label_ = new QLabel;
  status_label_->setStyleSheet("color: red");

  layout->addLayout(base_topic_layout);
  layout->addLayout(fpa_temp_layout);
  layout->addLayout(aux_temp_layout);
  layout->addStretch(1);
  layout->addLayout(gain_layout);
  layout->addLayout(shutter_mode_layout);
  layout->addLayout(bottom_buttons);
  layout->addStretch(1);
  layout->addWidget(status_label_);

  QTimer *refresh_temperatures(new QTimer);
  refresh_temperatures->setInterval(1000); // Every second
  connect(refresh_temperatures, &QTimer::timeout, this, [=]()
  {
    QtConcurrent::run(this, &Lepton::callGetTemperatures);
  });

  connect(change_base_topic_, &QPushButton::clicked, this, [ = ]()
  {
    base_topic_ = base_topic_line_edit_->text().toStdString();
    refresh_temperatures->stop();
    get_temperatures_client_ = nh_.serviceClient<flir_lepton::GetTemperatures>(base_topic_ + "/get_temperatures");
    refresh_temperatures->start();
  });

  connect(gain_mode, qOverload<int>(&QComboBox::currentIndexChanged), this, [ = ](const int index)
  {
    set_gain_mode_srv_.request.mode = index;
    QtConcurrent::run(this, &Lepton::callSetGainMode);
  });

  connect(shutter_mode, qOverload<int>(&QComboBox::currentIndexChanged), this, [ = ](const int index)
  {
    set_shutter_mode_srv_.request.mode = index;
    QtConcurrent::run(this, &Lepton::callSetShutterMode);
  });

  connect(run_ffc_normalization, &QPushButton::clicked, this, [ = ]()
  {
    QtConcurrent::run(this, &Lepton::callRunFfcNormalization);
  });

    connect(run_oem_reboot, &QPushButton::clicked, this, [ = ]()
  {
    QtConcurrent::run(this, &Lepton::callRunOemReboot);
  });

  connect(this, &Lepton::newFpaTemperature, this, [=](const double temperature)
  {
    fpa_temperature->setValue(temperature);
  });

  connect(this, &Lepton::newAuxTemperature, this, [=](const double temperature)
  {
    aux_temperature->setValue(temperature);
  });

  connect(this, &Lepton::newErrorStatusLabel, this, [=](const QString error)
  {
    status_label_->setText(error);
  });
  connect(this, &Lepton::clearStatusLabel, this, [=]()
  {
    status_label_->clear();
  });

  connect(fpa_temperature, qOverload<double>(&QDoubleSpinBox::valueChanged), this, [=](const double value)
  {
    if (value >= 80)
      fpa_temperature->setStyleSheet("color:red");
    else if (value >= 60)
      fpa_temperature->setStyleSheet("color: orange");
    else
      fpa_temperature->setStyleSheet("");
  });

  connect(aux_temperature, qOverload<double>(&QDoubleSpinBox::valueChanged), this, [=](const double value)
  {
    if (value >= 80)
      aux_temperature->setStyleSheet("color:red");
    else if (value >= 60)
      aux_temperature->setStyleSheet("color: orange");
    else
      aux_temperature->setStyleSheet("");
  });

  connect(change_base_topic_, &QPushButton::clicked, this, &Lepton::configChanged);
}

Lepton::~Lepton()
{
}

void Lepton::load(const rviz::Config &config)
{
  rviz::Panel::load(config);

  QString tmp_str;
  if (config.mapGetString("base_topic", &tmp_str))
    base_topic_line_edit_->setText(tmp_str);

  // To start the timer that fetches sensor temperature:
  Q_EMIT change_base_topic_->clicked();
}

void Lepton::save(rviz::Config config) const
{
  rviz::Panel::save(config);
  config.mapSetValue("base_topic", base_topic_line_edit_->text());
}

void Lepton::displayMessageBoxHandler(const QString title,
    const QString text,
    const QString info,
    const QMessageBox::Icon icon)
{
  const bool old_state(isEnabled());
  setEnabled(false);
  QMessageBox msg_box;
  msg_box.setWindowTitle(title);
  msg_box.setText(text);
  msg_box.setInformativeText(info);
  msg_box.setIcon(icon);
  msg_box.setStandardButtons(QMessageBox::Ok);
  msg_box.exec();
  setEnabled(old_state);
}

void Lepton::callSetGainMode()
{
  Q_EMIT enable(false);
  ros::ServiceClient client(nh_.serviceClient<flir_lepton::SetGainMode>(base_topic_ + "/set_gain_mode"));

  if (!client.waitForExistence(ros::Duration(1)))
  {
    Q_EMIT enable(true);
    Q_EMIT displayMessageBox("Error calling service",
                             QString::fromStdString("Service " + client.getService() + " does not exist"),
                             "", QMessageBox::Critical);
    return;
  }

  if (!client.call(set_gain_mode_srv_))
  {
    Q_EMIT enable(true);
    Q_EMIT displayMessageBox("Error calling service",
                             QString::fromStdString("Could not call service " + client.getService()),
                             "", QMessageBox::Critical);
    return;
  }

  if (!set_gain_mode_srv_.response.error.empty())
  {
    Q_EMIT enable(true);
    Q_EMIT displayMessageBox("Error calling service",
                             QString::fromStdString("Service " + client.getService() + " returned an error"),
                             QString::fromStdString(set_gain_mode_srv_.response.error),
                             QMessageBox::Critical);
    return;
  }

  Q_EMIT enable(true);
}

void Lepton::callSetShutterMode()
{
  Q_EMIT enable(false);
  ros::ServiceClient client(nh_.serviceClient<flir_lepton::SetShutterMode>(base_topic_ + "/set_shutter_mode"));

  if (!client.waitForExistence(ros::Duration(1)))
  {
    Q_EMIT enable(true);
    Q_EMIT displayMessageBox("Error calling service",
                             QString::fromStdString("Service " + client.getService() + " does not exist"),
                             "", QMessageBox::Critical);
    return;
  }

  if (!client.call(set_shutter_mode_srv_))
  {
    Q_EMIT enable(true);
    Q_EMIT displayMessageBox("Error calling service",
                             QString::fromStdString("Could not call service " + client.getService()),
                             "", QMessageBox::Critical);
    return;
  }

  if (!set_shutter_mode_srv_.response.error.empty())
  {
    Q_EMIT enable(true);
    Q_EMIT displayMessageBox("Error calling service",
                             QString::fromStdString("Service " + client.getService() + " returned an error"),
                             QString::fromStdString(set_shutter_mode_srv_.response.error),
                             QMessageBox::Critical);
    return;
  }

  Q_EMIT enable(true);
}

void Lepton::callRunFfcNormalization()
{
  Q_EMIT enable(false);
  ros::ServiceClient client(nh_.serviceClient<flir_lepton::RunFFCNormalization>(base_topic_ + "/run_ffc_normalization"));

  if (!client.waitForExistence(ros::Duration(1)))
  {
    Q_EMIT enable(true);
    Q_EMIT displayMessageBox("Error calling service",
                             QString::fromStdString("Service " + client.getService() + " does not exist"),
                             "", QMessageBox::Critical);
    return;
  }

  if (!client.call(run_ffc_normalization_srv_))
  {
    Q_EMIT enable(true);
    Q_EMIT displayMessageBox("Error calling service",
                             QString::fromStdString("Could not call service " + client.getService()),
                             "", QMessageBox::Critical);
    return;
  }

  if (!run_ffc_normalization_srv_.response.error.empty())
  {
    Q_EMIT enable(true);
    Q_EMIT displayMessageBox("Error calling service",
                             QString::fromStdString("Service " + client.getService() + " returned an error"),
                             QString::fromStdString(run_ffc_normalization_srv_.response.error),
                             QMessageBox::Critical);
    return;
  }

  Q_EMIT enable(true);
}

void Lepton::callRunOemReboot()
{
  Q_EMIT enable(false);
  ros::ServiceClient client(nh_.serviceClient<flir_lepton::RunOEMReboot>(base_topic_ + "/run_oem_reboot"));

  if (!client.waitForExistence(ros::Duration(1)))
  {
    Q_EMIT enable(true);
    Q_EMIT displayMessageBox("Error calling service",
                             QString::fromStdString("Service " + client.getService() + " does not exist"),
                             "", QMessageBox::Critical);
    return;
  }

  if (!client.call(run_oem_reboot_srv_))
  {
    Q_EMIT enable(true);
    Q_EMIT displayMessageBox("Error calling service",
                             QString::fromStdString("Could not call service " + client.getService()),
                             "", QMessageBox::Critical);
    return;
  }

  if (!run_oem_reboot_srv_.response.error.empty())
  {
    Q_EMIT enable(true);
    Q_EMIT displayMessageBox("Error calling service",
                             QString::fromStdString("Service " + client.getService() + " returned an error"),
                             QString::fromStdString(run_oem_reboot_srv_.response.error),
                             QMessageBox::Critical);
    return;
  }

  Q_EMIT enable(true);
  QtConcurrent::run(this, &Lepton::callSetShutterMode);
}


void Lepton::callGetTemperatures()
{
  flir_lepton::GetTemperatures srv;
  if (!get_temperatures_client_.waitForExistence(ros::Duration(0.8)))
  {
    std::string error("Service " + get_temperatures_client_.getService() + " does not exist");
    Q_EMIT newErrorStatusLabel(QString::fromStdString(error));
    return;
  }

  if (!get_temperatures_client_.call(srv))
  {
    std::string error("Could not call service " + get_temperatures_client_.getService());
    Q_EMIT newErrorStatusLabel(QString::fromStdString(error));
    return;
  }

  if (!srv.response.error.empty())
  {
    std::string error ("Service " + get_temperatures_client_.getService() +
                       " returned an error\n" + srv.response.error);
    Q_EMIT newErrorStatusLabel(QString::fromStdString(error));
    return;
  }

  Q_EMIT clearStatusLabel();
  Q_EMIT newFpaTemperature(srv.response.fpa_celcius);
  Q_EMIT newAuxTemperature(srv.response.aux_celcius);
}

}

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(flir_lepton::Lepton, rviz::Panel)

