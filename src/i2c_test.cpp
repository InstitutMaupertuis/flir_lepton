#include <atomic>
#include <flir_lepton/sdk/LEPTON_OEM.h>
#include <flir_lepton/sdk/LEPTON_SDK.h>
#include <flir_lepton/sdk/LEPTON_SYS.h>
#include <iostream>
#include <string>

using std::cout;
using std::cerr;
using std::endl;

std::atomic<bool> connected(false);
LEP_CAMERA_PORT_DESC_T port;

void lepton_connect()
{
  const LEP_RESULT r(LEP_OpenPort(1, LEP_CCI_TWI, 400, &port));
  if (r == LEP_OK)
  {
    connected = true;
    return;
  }

  throw std::runtime_error("Error connecting to I2C: " + std::to_string(r));
}

std::string gainModeToString(const LEP_SYS_GAIN_MODE_E &mode)
{
  if (mode == LEP_SYS_GAIN_MODE_HIGH)
    return "High";
  else if (mode == LEP_SYS_GAIN_MODE_LOW)
    return "Low";
  else if (mode == LEP_SYS_GAIN_MODE_AUTO)
    return "Auto";
  else
    throw std::runtime_error("Unknown gain mode");
}

int main(void)
{
  lepton_connect();
  LEP_RESULT r;

  LEP_OEM_PART_NUMBER_T part_number;
  r = LEP_GetOemFlirPartNumber(&port, &part_number);
  if (r == LEP_OK)
  {
    cout << "Part number: " << part_number.value << endl;
    std::string part_number_str(part_number.value);

    // Part numbers from FLIR Lepton Engineering Datasheet, section 1.5: Key Specifications
    if (part_number_str.compare("500-0659-01"))
      cout << "Lepton 2.0 shuttered" << endl;
    else if (part_number_str.compare("500-0763-01"))
      cout << "Lepton 2.5 shuttered (radiometric)" << endl;
    else if (part_number_str.compare("500-0726-01"))
      cout << "Lepton 3 shuttered" << endl;
    else if (part_number_str.compare("500-0771-01"))
      cout << "Lepton 3.5 shuttered (radiometric)" << endl;
    else
      cerr << "Unknown Lepton device" << endl;
  }
  else
    cerr << "Could not get Flir part number" << endl;


  LEP_SYS_FLIR_SERIAL_NUMBER_T serial_number;
  r = LEP_GetSysFlirSerialNumber(&port, &serial_number);
  if (r == LEP_OK)
    cout << "Serial number: " << serial_number << endl;
  else
    cerr << "Could not get serial number" << endl;

  LEP_SYS_GAIN_MODE_E gain_mode;
  r = LEP_GetSysGainMode(&port, &gain_mode);
  if (r == LEP_OK)
  {
    cout << "Gain mode: " << gain_mode << endl;
    cout << "Gain mode: " << gainModeToString(gain_mode) << endl;
  }
  else
    cerr << "Could not get gain mode" << endl;

//   LEP_SYS_GAIN_MODE_E gain_mode;
  gain_mode = LEP_SYS_GAIN_MODE_LOW;
  r = LEP_SetSysGainMode(&port, gain_mode);

  return 0;
}
