#include <atomic>
#include <cv_bridge/cv_bridge.h>
#include <fcntl.h>
#include <flir_lepton/GetTemperatures.h>
#include <flir_lepton/master.hpp>
#include <flir_lepton/RunFFCNormalization.h>
#include <flir_lepton/RunOEMReboot.h>
#include <flir_lepton/sdk_error_code_to_string.hpp>
#include <flir_lepton/sdk/LEPTON_OEM.h>
#include <flir_lepton/sdk/LEPTON_SDK.h>
#include <flir_lepton/sdk/LEPTON_SYS.h>
#include <flir_lepton/SetGainMode.h>
#include <flir_lepton/SetShutterMode.h>
#include <linux/spi/spidev.h>
#include <linux/types.h>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sys/ioctl.h>
#include <wiringPi.h>

using std::cout;
using std::cerr;
using std::endl;

// I2C - Configuration
std::atomic<bool> i2c_connected(false);
LEP_CAMERA_PORT_DESC_T port;

// SPI - Video
const char *device = "/dev/spidev0.1";
uint8_t mode(0);
uint8_t bits(8);
uint32_t speed(16000000); // Hz
uint16_t spi_delay(0);

const unsigned frame_width(80);
const unsigned frame_height(60);
const unsigned row_packet_words(frame_width + 2);
const unsigned row_packet_bytes(row_packet_words * 2);
const unsigned frame_words(frame_width *frame_height);

LEP_SYS_FFC_SHUTTER_MODE_OBJ_T shutter_mode;

void initFlirLeptonI2C();

bool setShutterModeCallback(flir_lepton::SetShutterModeRequest &req,
                            flir_lepton::SetShutterModeResponse &res)
{
  shutter_mode.shutterMode = LEP_SYS_FFC_SHUTTER_MODE_E_TAG(req.mode);
  const LEP_RESULT r(LEP_SetSysFfcShutterModeObj(&port, shutter_mode));
  if (r == LEP_OK)
    return true;
  else
    res.error = "Could not set FFC shutter mode object. " + lep_error_to_string(r) + " (" + std::to_string(r) + ")";

  return true;
}

bool runFccNormalizationCallback(flir_lepton::RunFFCNormalizationRequest &,
                                 flir_lepton::RunFFCNormalizationResponse &res)
{
  LEP_RESULT r;
  LEP_OEM_FFC_NORMALIZATION_TARGET_T target;
  r = LEP_GetOemFFCNormalizationTarget(&port, &target);
  if (r == LEP_OK)
    cout << "FFC normalization target: " << target << endl;
  else
    cerr << "Could not fetch FFC normalization target" << endl;

  r = LEP_RunSysFFCNormalization(&port);
  if (r == LEP_OK)
    return true;
  else
    res.error = "Could not set FFC shutter mode object. " + lep_error_to_string(r) + " (" + std::to_string(r) + ")";
  return true;
}

bool setGainModeCallback(flir_lepton::SetGainModeRequest &req,
                         flir_lepton::SetGainModeResponse &res)
{
  LEP_SYS_GAIN_MODE_E gain(LEP_SYS_GAIN_MODE_E_TAG(req.mode));
  const LEP_RESULT r(LEP_SetSysGainMode(&port, gain));
  if (r == LEP_OK)
    return true;
  else
    res.error = "Could not set gain mode. " + lep_error_to_string(r) + " (" + std::to_string(r) + ")";
  return true;
}

bool runOemRebootCallback(flir_lepton::RunOEMRebootRequest &,
                          flir_lepton::RunOEMRebootResponse &res)
{
  LEP_RESULT r(LEP_RunOemReboot(&port));
  if (r == LEP_OK)
    return true;
  else
    res.error = "Could not run OEM reboot. " + lep_error_to_string(r) + " (" + std::to_string(r) + ")";

  // Send shutter mode again because reboot resets the shutter mode
  r = LEP_SetSysFfcShutterModeObj(&port, shutter_mode);
  if (r == LEP_OK)
    return true;
  else
    res.error = "Could not set FFC shutter mode object. " + lep_error_to_string(r) + " (" + std::to_string(r) + ")";

  return true;
}

bool getTemperaturesCallback(flir_lepton::GetTemperaturesRequest&,
                          flir_lepton::GetTemperaturesResponse &res)
{
  LEP_RESULT r;

  LEP_SYS_FPA_TEMPERATURE_CELCIUS_T fpa_temp;
  r = LEP_GetSysFpaTemperatureCelcius(&port, &fpa_temp);
  if (r != LEP_OK)
  {
    res.error = "Could not get FPA temperature. " + lep_error_to_string(r) + " (" + std::to_string(r) + ")";
    return true;
  }

  LEP_SYS_AUX_TEMPERATURE_CELCIUS_T aux_temp;
  r = LEP_GetSysAuxTemperatureCelcius(&port, &aux_temp);
  if (r != LEP_OK)
  {
    res.error = "Could not get AUX temperature. " + lep_error_to_string(r) + " (" + std::to_string(r) + ")";
    return true;
  }

  res.fpa_celcius = fpa_temp;
  res.aux_celcius = aux_temp;
  return true;
}

void initFlirLeptonI2C()
{
  // I2C bus
  const LEP_RESULT r(LEP_OpenPort(1, LEP_CCI_TWI, 400, &port));
  if (r == LEP_OK)
  {
    i2c_connected = true;
    return;
  }

  throw std::runtime_error("Error connecting to I2C: " + std::to_string(r));
}

int initFlirLeptonSPI()
{
  // SPI bus
  int fd(open(device, O_RDWR));
  if (fd < 0)
  {
    cerr << "Can't open SPI device" << endl;
    return -1;
  }

  int ret(ioctl(fd, SPI_IOC_WR_MODE, &mode));
  if (ret == -1)
  {
    cerr << "Can't set SPI mode" << endl;
    return -1;
  }

  ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
  if (ret == -1)
  {
    cerr << "Can't get SPI mode" << endl;
    return -1;
  }

  ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
  if (ret == -1)
  {
    cerr << "Can't set bits per word" << endl;
    return -1;
  }

  ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
  if (ret == -1)
  {
    cerr << "Can't get bits per word" << endl;
    return -1;
  }

  ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
  if (ret == -1)
  {
    cerr << "Can't set max speed hz" << endl;
    return 1;
  }

  ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
  if (ret == -1)
  {
    cerr << "Can't get max speed hz" << endl;
    return -1;
  }

  return fd;
}

void captureImages(ros::NodeHandle &nh,
                   const int &fd,
                   cv_bridge::CvImage &raw,
                   ros::Publisher &pub)
{
  uint8_t tx[row_packet_bytes];
  uint8_t result[row_packet_bytes * frame_height];
  uint16_t raw_data_[frame_words];

  uint16_t spi_error_count(0);

  struct spi_ioc_transfer tr;
  tr.tx_buf = (unsigned long)tx;
  tr.len = (unsigned long)row_packet_bytes;
  tr.delay_usecs = spi_delay;
  tr.speed_hz = speed;
  tr.bits_per_word = bits;

  int resets(0); // Number of times we've reset the 0...59 loop for packets
  int errors(0); // Number of error-packets received

  ros::Rate r(9); // Hz
  while (nh.ok() && ros::master::check() && ros::ok())
  {
    int i_row;
    for (i_row = 0; (unsigned) i_row < frame_height;)
    {
      uint8_t *packet = &result[i_row * row_packet_bytes];
      tr.rx_buf = (unsigned long) packet;
      if (ioctl(fd, SPI_IOC_MESSAGE(1), &tr) < 1)
      {
        cerr << "Error transferring SPI packet" << endl;
        ros::Duration(0.5).sleep();
        continue;
      }

      int packet_number;
      if ((packet[0] & 0xf) == 0xf)
      {
        packet_number = -1;
      }
      else
      {
        packet_number = packet[1];
      }

      if (packet_number == -1)
      {
        usleep(1000);
        if (++errors > 300)
        {
          break;
        }
        continue;
      }

      if (packet_number != i_row)
      {
        usleep(1000);
        break;
      }

      ++i_row;
    }

    if ((unsigned) i_row < frame_height)
    {
      if (++resets >= 750)
      {
        resets = 0;
        ++spi_error_count;
        cerr << "Lost VoSPI sync " << spi_error_count << endl;
        ros::Duration(1).sleep();
        /*
        cerr << "OEM reboot" << endl;
        flir_lepton::RunOEMReboot srv_1;
        runOemRebootCallback(srv_1.request, srv_1.response);
        ros::Duration(0.5).sleep();
        cerr << "Set shutter mode (external)" << endl;
        flir_lepton::SetShutterMode srv_2;
        srv_2.request.mode = 2;
        setShutterModeCallback(srv_2.request, srv_2.response);
        cerr << "VoSPI sync reset" << endl;
        */
      }
      continue;
    }

    resets = 0;
    errors = 0;

    uint16_t min_value((1 << 16) - 1);
    uint16_t max_value(0);
    uint8_t *in = &result[0];
    uint16_t *out = &raw_data_[0];
    for (int i_row(0); (unsigned) i_row < frame_height; ++i_row)
    {
      in += 4;
      for (int i_col(0); (unsigned) i_col < frame_width; ++i_col)
      {
        // Convert to CV image
        raw.image.at<uint16_t>(i_row, i_col) = raw_data_[i_row * frame_width + i_col];

        uint16_t value = in[0];
        value <<= 8;
        value |= in[1];
        in += 2;

        if (value > max_value)
          max_value = value;
        if (value < min_value)
          min_value = value;

        *(out++) = value;
      }
    }

    raw.header.stamp = ros::Time::now();
    pub.publish(raw);
    ++raw.header.seq;
    r.sleep();
  }
}

int main(int argc, char *argv[])
{
  wiringPiSetup();
  pinMode(2, OUTPUT); // Pin wired to the transistor
  digitalWrite(2, HIGH); // Enable Flir Lepton

  initFlirLeptonI2C();
  int fd(initFlirLeptonSPI());
  if (fd < 0)
  {
    cerr << "Failed to initialize Flir Lepton sensor" << endl;
    return 1;
  }
  usleep(5e5); // 0.5 sec

  LEP_RESULT r;

  LEP_OEM_PART_NUMBER_T part_number;
  r = LEP_GetOemFlirPartNumber(&port, &part_number);
  if (r == LEP_OK)
  {
    cout << "Part number: " << part_number.value << endl;
    std::string part_number_str(part_number.value);

    // Part numbers from FLIR Lepton Engineering Datasheet, section 1.5: Key Specifications
    if (part_number_str.compare("500-0659-01"))
      cout << "Lepton 2.0 shuttered" << endl;
    else if (part_number_str.compare("500-0763-01"))
      cout << "Lepton 2.5 shuttered (radiometric)" << endl;
    else if (part_number_str.compare("500-0726-01"))
      cout << "Lepton 3 shuttered" << endl;
    else if (part_number_str.compare("500-0771-01"))
      cout << "Lepton 3.5 shuttered (radiometric)" << endl;
    else
      cerr << "Unknown Lepton device" << endl;
  }
  else
    cerr << "Could not get Flir part number" << endl;

  LEP_SYS_FLIR_SERIAL_NUMBER_T serial_number;
  r = LEP_GetSysFlirSerialNumber(&port, &serial_number);
  if (r == LEP_OK)
    cout << "Serial number: " << serial_number << endl;
  else
    cerr << "Could not get serial number" << endl;

  // Set manual shutter mode
  cout << "Set shutter mode (external)" << endl;
  flir_lepton::SetShutterMode srv_2;
  srv_2.request.mode = LEP_SYS_FFC_SHUTTER_MODE_MANUAL;
  setShutterModeCallback(srv_2.request, srv_2.response);

  LEP_OEM_FFC_NORMALIZATION_TARGET_T target;
  r = LEP_GetOemFFCNormalizationTarget(&port, &target);
  if (r == LEP_OK)
    cout << "FFC normalization target: " << target << endl;
  else
    cerr << "Could not fetch FFC normalization target" << endl;

  bool reconnect(false);
  do
  {
    // Don't use ROS_*_STREAM here, it does not work after master re-connect
    RosConnectionHandler rc(argc, argv, "flir_lepton");
    std::string raw_topic("raw");
    rc.nh_->getParam("raw_topic", raw_topic);
    ros::Publisher pub = rc.nh_->advertise<sensor_msgs::Image>(raw_topic, 5);
    ros::ServiceServer srv_set_shutter_mode = rc.nh_->advertiseService("set_shutter_mode", &setShutterModeCallback);
    ros::ServiceServer srv_run_ffc_normalization = rc.nh_->advertiseService("run_ffc_normalization", &runFccNormalizationCallback);
    ros::ServiceServer srv_set_gain_mode = rc.nh_->advertiseService("set_gain_mode", &setGainModeCallback);
    ros::ServiceServer srv_run_oem_reboot = rc.nh_->advertiseService("run_oem_reboot", &runOemRebootCallback);
    ros::ServiceServer srv_get_temperatures = rc.nh_->advertiseService("get_temperatures", &getTemperaturesCallback);
    ros::AsyncSpinner s(0);
    s.start();

    cv_bridge::CvImage raw;
    raw.encoding = sensor_msgs::image_encodings::MONO16;
    raw.header.frame_id = "flir_lepton";
    raw.header.seq = 0;
    raw.header.stamp = ros::Time::now();
    raw.image = cv::Mat::zeros(60, 80, CV_16U); // rows (height), cols (width)

    cout << "Starting to publish raw images" << endl;
    captureImages(*rc.nh_, fd, raw, pub);

    cerr << "Reconnecting to master..." << endl;
    reconnect = !ros::master::check() && ros::ok();
  }
  while (reconnect);
}
