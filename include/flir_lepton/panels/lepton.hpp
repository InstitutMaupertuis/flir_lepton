#ifndef FLIR_LEPTON_PANELS_LEPTON_HPP
#define FLIR_LEPTON_PANELS_LEPTON_HPP

#include <flir_lepton/GetTemperatures.h>
#include <flir_lepton/RunFFCNormalization.h>
#include <flir_lepton/RunOEMReboot.h>
#include <flir_lepton/SetGainMode.h>
#include <flir_lepton/SetShutterMode.h>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QScrollArea>
#include <QtConcurrent/QtConcurrent>
#include <QTimer>
#include <QVBoxLayout>
#include <ros/ros.h>
#include <rviz/panel.h>

namespace flir_lepton
{

class Lepton : public rviz::Panel
{
  Q_OBJECT

public:
  Lepton(QWidget *parent = NULL);
  virtual ~Lepton();

Q_SIGNALS:
  void enable(const bool);
  void displayMessageBox(const QString,
                         const QString,
                         const QString,
                         const QMessageBox::Icon);
  void newFpaTemperature(const double);
  void newAuxTemperature(const double);
  void newErrorStatusLabel(const QString);
  void clearStatusLabel();

protected Q_SLOTS:
  virtual void load(const rviz::Config &config);
  virtual void save(rviz::Config config) const;
  void displayMessageBoxHandler(const QString title,
                                const QString text,
                                const QString info = "",
                                const QMessageBox::Icon icon = QMessageBox::Icon::Information);

private:
  void callSetGainMode();
  void callSetShutterMode();
  void callRunFfcNormalization();
  void callRunOemReboot();
  void callGetTemperatures();

  ros::NodeHandle nh_;
  std::string base_topic_;
  flir_lepton::SetGainMode set_gain_mode_srv_;
  flir_lepton::RunFFCNormalization run_ffc_normalization_srv_;
  flir_lepton::SetShutterMode set_shutter_mode_srv_;
  flir_lepton::RunOEMReboot run_oem_reboot_srv_;
  ros::ServiceClient get_temperatures_client_;

  QLineEdit* base_topic_line_edit_;
  QPushButton *change_base_topic_;
  QLabel *status_label_;
};

}

#endif
