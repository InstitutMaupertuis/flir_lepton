#ifndef FLIR_LEPTON_MASTER_HPP
#define FLIR_LEPTON_MASTER_HPP

#include <ros/ros.h>
#include <string>
#include <memory>

class RosConnectionHandler
{
public:
  RosConnectionHandler(int argc,
                       char** argv,
                       std::string node_name)
  {
    ros::init(argc, argv, node_name);
    nh_.reset(new ros::NodeHandle("~"));
  }

  ~RosConnectionHandler()
  {
  }

  std::shared_ptr<ros::NodeHandle> nh_;
};

#endif
