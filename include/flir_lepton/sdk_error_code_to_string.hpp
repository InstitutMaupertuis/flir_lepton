#ifndef SDK_ERROR_CODE_TO_STRING_HPP
#define SDK_ERROR_CODE_TO_STRING_HPP

#include <string>
#include <flir_lepton/sdk/LEPTON_ErrorCodes.h>

const std::string lep_error_to_string(const LEP_RESULT &e)
{
  switch (e)
  {
    case LEP_OK:
      return "";
//     case LEP_COMM_OK:
//       return "";
    case LEP_ERROR:
      return "Camera general error";
    case LEP_NOT_READY:
      return "Camera not ready error";
    case LEP_RANGE_ERROR:
      return "Camera range error";
    case LEP_CHECKSUM_ERROR:
      return "Camera checksum error";
    case LEP_BAD_ARG_POINTER_ERROR:
      return "Camera bad argument error";
    case LEP_DATA_SIZE_ERROR:
      return "Camera byte count error";
    case LEP_UNDEFINED_FUNCTION_ERROR:
      return "Camera undefined function error";
    case LEP_FUNCTION_NOT_SUPPORTED:
      return "Camera function not yet supported error";
    case LEP_DATA_OUT_OF_RANGE_ERROR:
      return "Camera input DATA is out of valid range error";
    case LEP_COMMAND_NOT_ALLOWED:
      return "Camera unable to execute command due to current camera state";
    case LEP_OTP_WRITE_ERROR:
      return "Camera OTP write error";
    case LEP_OTP_READ_ERROR:
      return "OTP: double bit error detected (uncorrectible)";
    case LEP_OTP_NOT_PROGRAMMED_ERROR:
      return "OTP: Flag read as non-zero";
    case LEP_ERROR_I2C_BUS_NOT_READY:
      return "I2C Bus Error - Bus not available";
    case LEP_ERROR_I2C_BUFFER_OVERFLOW:
      return "I2C Bus Error - Buffer overflow";
    case LEP_ERROR_I2C_ARBITRATION_LOST:
      return "I2C Bus Error - Bus arbitration lost";
    case LEP_ERROR_I2C_BUS_ERROR:
      return "I2C Bus Error - General bus error";
    case LEP_ERROR_I2C_NACK_RECEIVED:
      return "I2C Bus Error - NACK received";
    case LEP_ERROR_I2C_FAIL:
      return "I2C Bus Error - General failure";
    case LEP_DIV_ZERO_ERROR:
      return "Attempted div by zero";
    case LEP_COMM_PORT_NOT_OPEN:
      return "Comm port not open";
    case LEP_COMM_INVALID_PORT_ERROR:
      return "Comm port no such port error";
    case LEP_COMM_RANGE_ERROR:
      return "Comm port range error";
    case LEP_ERROR_CREATING_COMM:
      return "Error creating comm";
    case LEP_ERROR_STARTING_COMM:
      return "Error starting comm";
    case LEP_ERROR_CLOSING_COMM:
      return "Error closing comm";
    case LEP_COMM_CHECKSUM_ERROR:
      return "Comm checksum error";
    case LEP_COMM_NO_DEV:
      return "No comm device";
    case LEP_TIMEOUT_ERROR:
      return "Comm timeout error";
    case LEP_COMM_ERROR_WRITING_COMM:
      return "Error writing comm";
    case LEP_COMM_ERROR_READING_COMM:
      return "Error reading comm";
    case LEP_COMM_COUNT_ERROR:
      return "Comm byte count error";
    case LEP_OPERATION_CANCELED:
      return "Camera operation canceled";
    case LEP_UNDEFINED_ERROR_CODE:
      return "Undefined error";
    default:
      return "Unknown error";
  }

  return "";
}

#endif
