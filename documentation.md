- Le Raspberry Pi récupère les images de la caméra thermique et les envoie au PC via ROS
- Le Raspberry Pi devrait se reconnecter automatiquement si le PC est déconnecté / reconnecté du réseau de la C2

# Se connecter en SSH au Raspberry Pi

```bash
ssh im@pi-thermal-camera
```

## Statut caméra thermique

```bash
systemctl status ros_flir_lepton
```

Log:

```bash
journalctl -u ros_flir_lepton | tail -n30
```

## Vérifier publication images
On doit avoir environ 9 images par seconde

```bash
rostopic hz -w 2 /ram/sensors/thermal_camera/lepton_2_5/raw
```

## Redémarrer caméra thermique

```bash
rosservice call /flir_lepton/run_oem_reboot "{}"
```

Si ça ne fonctionne pas:

```bash
sudo systemctl restart ros_flir_lepton
```

En dernier recours débrancher/rebrancher la caméra si elle ne publie pas d'images
