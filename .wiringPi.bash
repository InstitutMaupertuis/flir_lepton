#!/bin/bash
git clone https://github.com/WiringPi/WiringPi.git
cd WiringPi
sed -i "s/\${WIRINGPI_SUDO-sudo}/\'\'/g" build # We don't have sudo installed
./build
ldconfig
cd ..

